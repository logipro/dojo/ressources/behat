# BeHat

Découvrir Behat

# Installation

```bash
git clone git@gitlab.com:logipro/dojo/ressources/behat.git
cd behat
bin/composer install
```

# Lancement

Lancement des scénarios d'acceptation de behat.
```bash
bin/behat
```
Ces scénarios sont localisés dans tests/acceptation.

**Vous pouvez observer le fichier log/scenarios.log qui se remplit à chaque exectuion de behat**

N'hésitez pas à ouvrir ce fichier dans votre éditeur et en vider le contenu pour mieux observer le comportement de behat

## Les scenarios

Une premiere feature concernant 2 scenario de bases permettant de prendre en main les étapes (step).
```bash
bin/behat tests/acceptation/01-ExemplePremierScenarioContext.feature
```

bin/behat tests/acceptation/01-ExemplePremierScenarioContext.feature 

# Tests pour les développeurs de ce projet

Vérifications du typage, de la syntaxe, du passage des tests unitaires.

```bash
./codecheck
```

