<?php

namespace Dojo;

class Log extends LoggerFichier
{
    private static string $filename = __DIR__.'/../log/scenarios.log';

    public function __construct()
    {
        parent::__construct(self::$filename);
    }
    public static function log(string $message):void
    {
        
        $logger = new LoggerFichier(self::$filename);
        $logger->ecrire($message);
    }
}
