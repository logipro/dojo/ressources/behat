<?php

namespace Dojo;

class Fichier implements PeripheriqueSortie
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function ecrire(string $message):void
    {
        $message.= "\n";
        try {
            $fp = fopen($this->filename, 'a');
            /** @var resource $fp */
            fwrite($fp, $message);
            fclose($fp);
        } catch (\Exception $e) {
            throw new \Exception("Impossible d'ouvrir en ecriture le fichier:".$this->filename);
        }
    }

    public function lire():string
    {
        $contenu = '';
        try {
            $fp = fopen($this->filename, 'r');
            /** @var resource $fp */
            while (!feof($fp)) {
                $contenu .= fread($fp, 8192);
            }
            fclose($fp);
        } catch (\Exception $e) {
            throw new \Exception("Fichier illisible:".$this->filename);
        }

        return $contenu;
    }
}
