<?php

namespace Dojo;

interface PeripheriqueSortie
{
    public function ecrire(string $message):void;
    public function lire():string;
}
