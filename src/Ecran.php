<?php

namespace Dojo;

class Ecran implements PeripheriqueSortie
{
    private string $buffer;

    public function __construct()
    {
        $this->buffer = '';
    }

    public function ecrire(string $message):void
    {
        $this->buffer = $message;
        echo($this->buffer);
    }

    public function lire():string
    {
        return $this->buffer;
    }
}
