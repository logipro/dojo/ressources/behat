<?php

namespace Dojo;

class LoggerFichier extends Logger
{
  
    public function __construct(string $filename = 'monlog.log')
    {
        $fichier = new Fichier($filename);
        parent::__construct($fichier);
    }
}
