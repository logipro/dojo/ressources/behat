<?php

namespace Dojo;

class Logger
{
    private PeripheriqueSortie $sortie;

    public function __construct(PeripheriqueSortie $sortie)
    {
        $this->sortie = $sortie;
    }

    public function ecrire(string $message):void
    {
        $this->sortie->ecrire($message);
    }

    public function lire():string
    {
        return $this->sortie->lire();
    }
}
