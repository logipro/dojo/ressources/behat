<?php

namespace Dojo\Tests;

use Dojo\Fichier;
use Exception;
use PHPUnit\Framework\TestCase;

class FichierTest extends TestCase
{
    public function testEcrire():void
    {
        $filename = sys_get_temp_dir()."/fichiertest.log";
        $ecran = new Fichier($filename);
        $message = "Un message".uniqid();
        $ecran->ecrire($message);

        $contenu = '';
        $fp = fopen($filename, 'r');
        if ($fp !== false) {
            while (!feof($fp)) {
                $contenu .= fread($fp, 8192);
            }
            fclose($fp);
        }

        $this->assertStringContainsString($message, $contenu);

        unlink($filename);
    }

    public function testLire():void
    {
        $filename = sys_get_temp_dir()."/fichiertest.log";
        $ecran = new Fichier($filename);
        $message = "Un message".uniqid();
        $ecran->ecrire($message);

        $contenu = $ecran->lire();

        $this->assertStringContainsString($message, $contenu);
        unlink($filename);
    }

    public function testEcrireExceptionOuvrirFichier():void
    {
        $this->expectException(\Exception::class);
        $filename = "/dossierinexistant/fichiertest.log";
        
        $ecran = new Fichier($filename);
        $message = "Un message".uniqid();
        $ecran->ecrire($message);
    }

    public function testLireExceptionOuvrirFichier():void
    {
        $this->expectException(\Exception::class);
        $filename = "/dossierinexistant/fichiertest.log";
        
        $ecran = new Fichier($filename);
        $ecran->lire();
    }
}
