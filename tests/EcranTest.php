<?php

namespace Dojo\Tests;

use Dojo\Ecran;
use PHPUnit\Framework\TestCase;

class EcranTest extends TestCase
{
    public function testEcrire():void
    {
        $ecran = new Ecran();
        ob_start();
        $ecran->ecrire("Un message");
        $output = ob_get_contents();
        $this->assertSame($output, "Un message");
        ob_end_clean();
    }

    public function testLire():void
    {
        $ecran = new Ecran();
        $message = "Un message";
        $ecran->ecrire($message);
        $contenu = $ecran->lire();
        $this->assertSame($contenu, $message);
    }
}
