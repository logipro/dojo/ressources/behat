<?php

namespace Dojo\Tests;

use Dojo\LoggerFichier;
use PHPUnit\Framework\TestCase;

class LoggerFichierTest extends TestCase
{
    public function testEcrire():void
    {
        $filename = sys_get_temp_dir()."/fichiertest.log";
        $log = new LoggerFichier($filename);
        $message = "Un message".uniqid();
        $log->ecrire($message);

        $contenu = '';
        $fp = fopen($filename, 'r');
        if ($fp !== false) {
            while (!feof($fp)) {
                $contenu .= fread($fp, 8192);
            }
            fclose($fp);
        }

        $this->assertStringContainsString($message, $contenu);
    }
}
