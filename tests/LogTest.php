<?php

namespace Dojo\Tests;

use Dojo\Log;
use PHPUnit\Framework\TestCase;

class LogTest extends TestCase
{
    public function testLog():void
    {
        $message = "Mon message".uniqid();
        Log::log($message);
        $log = new Log();
        $contenu = $log->lire();

        $this->assertStringContainsString($message, $contenu);
    }
}
