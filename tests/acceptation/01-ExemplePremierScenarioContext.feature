# language: fr
Fonctionnalité: feature minimaliste
  ETQ developpeur
  Je peux observer une feature minimaliste avec un contexte et quelques scénarios
  Afin de mieux comprendre gherkin et behat

  Contexte:
    Etant donné que nous avons des données:
        | Nom     | Prénom    |
        | Haddock | Archibald |
        | Hergé   | Tintin    |

  Scénario: je decouvre les étapes obligatoires du scénario
    Etant donné que j'écris la première étape
    Lorsque j'écris la deuxieme étape
    Alors cette troisiemes étape termine ce scénario minimaliste

  Scénario: je decouvre l'ecriture de d'étape / step / pas reutilisables
    Etant donné que j'écris l'étape "étant donné que"
    Et que j'écris l'étape "et que"
    Lorsque j'écris l'etape "lorsque"
    Et j'écris l'etape "et"
    Alors j'écris l'etape "alors"
    Mais j'écris l'etape "mais"
