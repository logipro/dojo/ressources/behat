<?php

namespace Features\Context;

class UserInMemory
{
    /** @var array<string,User> */
    private array $users = [];

    public function add(User $user): void
    {
        $this->users[$user->getNom()] = $user;
    }

    public function getByNom(string $nom):User
    {
        return $this->users[$nom];
    }
}
