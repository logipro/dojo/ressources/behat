<?php

namespace Features\Context;

use Behat\Behat\Context\Context;
use Dojo\Log;

class F02RecuperationDonneesDuContexteContext implements Context
{
    private UserInMemory $userRepository;
    public function __construct()
    {
        Log::log(__METHOD__);
        $this->userRepository = new UserInMemory();
        //$this->userRepository->add(new User($row['Nom'], $row['Prénom']));
        //         $this->userRepository->add(new User('Tournesol', 'Tryphon'));
        //         Log::log(print_r($this->userRepository, true));
    }
}
