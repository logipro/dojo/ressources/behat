<?php

namespace Features\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Dojo\Log;
use PHPUnit\Framework\Assert;

class F01ExemplePremierScenarioContext implements Context
{

    public function __construct()
    {
        Log::log("");
        Log::log("------=== Bienvenue dans le constructeur de ce 1er scenario Gherkin executé par behat ===------");
        Log::log(__METHOD__);
        Log::log("Nous voici dans le constructeur d'un feature.");
        Log::log(<<<FIN
        A chaque lancement de behat, le fichier feature est lu et pour chaque étape/step correspond 
        une fonction dans un fichier Context.php

        Vous observerez  aussi que pour chaque scenario de la feature, le contexte est relancé. Pour preuve,
        ces symboles : $?€ placés dans le constructeur de contexte va apparaitre autant de fois qu'il y a de scénarios.
        FIN);
        Log::log("------Fin du constructeur------");
    }
    /**
     * @Given nous avons des données:
     */
    public function nousAvonsDesDonnees(TableNode $table):void
    {
        Log::log("");
        Log::log(__METHOD__);
        Log::log("Affichons les données contenus dans le Gherkin:");
        /** @var array<string,string> $row */
        foreach ($table as $row) {
            Log::log($row['Nom'] ." ". $row['Prénom']);
        }
    }

    /**
     * @Given j'écris la première étape
     */
    public function jecrisLaPremiereEtape():void
    {
        Log::log("");
        Log::log(__METHOD__);
        Log::log("oui, j'écris la 1ère étape");
    }

    /**
     * @When j'écris la deuxieme étape
     */
    public function jecrisLaDeuxiemeEtapeAvecUnNomDeFonctionDifferentDeCeluiDeLetape():void
    {
        Log::log("");
        Log::log(__METHOD__);
        Log::log("oui, j'écris la 2ème étape");
        Log::log(<<<FIN
        Vous pourrez remarquer que le nom de la fonction ne correspond pas forcement à l'annotation.
        /**
         * @When j'écris la deuxieme étape
         */
        FIN);
    }

    /**
     * @Then cette troisiemes étape termine ce scénario minimaliste
     */
    public function cetteTroisiemesEtapeTermineCeScenarioMinimaliste():void
    {
        Log::log("");
        Log::log("oui, j'écris la 3ème étape obligatoire");
    }

    /**
     * @Given j'écris l'étape :arg1
     */
    public function jecrisLetape(string $arg1):void
    {
        Log::log("");
        Log::log(__METHOD__);
        Log::log("Passage d'un argument:".$arg1);
        Log::log("Vous remarquerez que les étapes @Given sont traitées independemment des autres @when et @then ");
    }

    /**
     * @When j'écris l'etape :jeChangeLeNomDeLArgument
     */
    public function jecrisLetape2(string $jeChangeLeNomDeLArgument):void
    {
        Log::log("");
        Log::log(__METHOD__);
        Log::log("Passage d'un argument (remarquez dans le code j'ai changé le nom):".$jeChangeLeNomDeLArgument);
        Log::log("Remarquez que dans le Gherkin, l'etape étant ecrite 4 fois, il existe 1 seule fonction
dans le contexte. pour les @when et les @then (mais pas les @given comme déjà remarqué ci dessus.");
    }
}
