<?php

namespace Dojo\Tests;

use Dojo\Ecran;
use Dojo\Logger;
use PHPUnit\Framework\TestCase;

class LoggerTest extends TestCase
{
    public function testEcrire():void
    {
        $sortie = new Ecran();
        $log = new Logger($sortie);
        $log->ecrire("Mon texte");
        $contenu = $sortie->lire();
        $this->assertStringContainsString("Mon texte", $contenu);
    }
}
